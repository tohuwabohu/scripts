#!/bin/bash
set -e

find . -maxdepth 1 -type f -printf %f\\n | grep -v "[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}.*" | while read file; do
  creation_time_in_seconds=$(stat -c %Y "$file") 
  creation_time_formatted=$(date --date="@$creation_time_in_seconds" '+%F')
  new_filename="$creation_time_formatted $file"
  if [ "$new_filename" != "$file" ]; then
    mv "$file" "$new_filename"
  fi
done
