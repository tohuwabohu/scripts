#!/bin/bash

function die {
  echo "error: $1"
  exit 1
}

test $# -gt 0 || die "usage: $0 <folder>"

TIMESTAMP=$(date '+%F_%H-%M')
LOG="$TIMESTAMP.log"
test -e $LOG || die "log file already existing: $LOG"

# -r recursive
# -e error-check
# -B ignore bitrate switching and enable VBR support
# -S ignore junk before first frame
# -E ignore junk after last frame
# -3 only mp3 and MP3
mp3check -r -e -B -S -E -3 -s $1 | grep -iv "valid id3 tag trailer" | tee $LOG
