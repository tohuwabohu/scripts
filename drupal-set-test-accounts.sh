#!/bin/bash

PASSWORD='$S$DItg.JeEB0lxk2SxZrkA93YGLhANWR9vPAK9FIKy4fmZa/HNfzqd'

MYSQL=$(which mysql)
MYSQL_ARGS="--host=localhost --user=dresdencup --password=dresdencup"

echo "setting all passwords to '$PASSWORD'"
$MYSQL $MYSQL_ARGS --execute="UPDATE drupal_users SET pass = '${PASSWORD}';" dresdencup
