#!/bin/bash

source "/usr/local/src/common-scripts/common.sh"
test "$#" -eq 2 || die "usage: $0 <ac3-in-file> <mp3-out-file>"
test -f "$1" || die "file not found: $1"
test ! -f "$2" || die "file already existing: $2"

ffmpeg -i "$1" -acodec libmp3lame -ab 192k "$2"
