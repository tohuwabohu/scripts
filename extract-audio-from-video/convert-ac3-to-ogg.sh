#!/bin/bash

source "/usr/local/src/common-scripts/common.sh"
test "$#" -eq 2 || die "usage: $0 <ac3-in-file> <ogg-out-file>"
test -f "$1" || die "file not found: $1"
test ! -f "$2" || die "file already existing: $2"

# -aq 6 - for lossless stereo coupling
ffmpeg -i "$1" -acodec libvorbis -aq 6 "$2"
