#!/bin/bash

source "/usr/local/src/common-scripts/common.sh"
test "$#" -eq 2 || die "usage: $0 <dvd-iso-file> <ac3-out-file>"
test -f "$1" || die "file not found: $1"
test ! -f "$2" || die "file already existing: $2"

mplayer dvd://1 -dvd-device "$1" -alang de -dumpaudio -dumpfile "$2"
