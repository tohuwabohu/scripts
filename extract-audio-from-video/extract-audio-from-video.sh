#!/bin/bash

set -e

# only split on newlines
IFS=$'\n'
for file in $(find . -name "*.flv" -o -name "*.mp4");do
  avconv -i "$file" -loglevel warning -acodec copy -vn "${file%.*}.m4a"
done
