#!/bin/bash

set -e

# only split on newlines
IFS=$'\n'
for file in $(find . -name "*.flv" -o -name "*.mp4");do
  avconv -i "$file" -loglevel warning -ab 192k -acodec libmp3lame -vn "${file%.*}.mp3"
done
