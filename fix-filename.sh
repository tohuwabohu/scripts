#!/bin/bash

# alias fixunix2dos='fix-filename.sh UTF-8 ISO-8859-1'
# alias fixdos2unix='fix-filename.sh ISO-8859-1 UTF-8'
# alias translit='fix-filename.sh UTF-8 ASCII//TRANSLIT'

if [ $# -lt 2 ]; then
  echo "run: $0 %from-encoding% %to-encoding% [now]"
  echo "e.g. $0 UTF-8 ISO-8859-1"
  exit 1
fi

function die {
  echo -e "[$(date '+%F_%H-%M')] $1"
  exit 1
}

# $1 from-encoding
# $2 to-encoding
# $3 filename to be converted
# $4 flag indicating dry-run or not
function convert {
  if [ ! -e "$3" ]; then
    die "file is not existing: $3"
  fi

  filename=$(basename "$3")
  dirname=$(dirname "$3")
  converted=$(echo "$filename" | iconv -f $1 -t $2) || die "failed to convert name: $dirname/$filename"
  if [ -z "$converted" ]; then
    die "conversion results in empty string: $dirname/$filename"
  fi

  if [ "$converted" != "$filename" ]; then
    if [ -e "$converted" ]; then
      die "file is already existing: $dirname/$converted"
    fi

    echo "$dirname/$filename => $dirname/$converted"
    if [ "$4" = "now" ]; then
      mv "$dirname/$filename" "$dirname/$converted" || die "failed to rename: \"$dirname/$filename\" to \"$dirname/$converted\""
    fi
  fi
}

echo "converting from $1 to $2"
if [ $# -lt 3 -o "$3" != "now" ]; then
  echo -e "(running in dry-mode only; run with 'now' option for persistent changes)"
fi

echo -e "\n"
find . -depth -print0 | while read -d $'\0' name; do
  if [ "$name" != "." -a "$name" != ".." ]; then
    convert $1 $2 "$name" $3
  fi
done
