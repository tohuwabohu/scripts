#!/bin/bash
set -e

source "/usr/local/lib/common-scripts/common.sh"

function print_usage {
  die "usage: $0 /path/to/git/repository"
}

test -n "$1" || print_usage
test -d "$1" || directory_not_found "$1"
test -d "$1/.git" || die "not a git repository: $1"

cd "$1" || die "Failed to change directory to: $1"

DIRTY=$(git status -s) || die "Failed to check repository for uncommited changes: $1"
test -z "$DIRTY" || die "Repository contains uncommited changes: $1"
git remote | grep -q origin || die "Repository has no remote origin: $1"
git branch | grep -q "* master" || die "Repository is not on master branch: $1"

git fetch --quiet origin || die "Failed to fetch changes from origin"
CHANGES=$(git log ..origin/master) || die "Failed to check origin/master for changes"
if [ -z "$CHANGES" ]; then
  echo "Already up-to-date."
  exit 0
fi

CURRENT_COMMIT=$(git rev-parse HEAD)
echo "--- current version ---"
git log -n 1 $CURRENT_COMMIT
 
echo "--- incoming changes ---"
git diff --name-only master origin/master
echo -e "---"
 
ask_yes_no "Apply changes?" || die "Operation canceled."
git merge --ff-only origin/master || die "Failed to merge changes from origin"
