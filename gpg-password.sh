#!/bin/bash -e

function die {
  echo "$1" 1>&2
  exit 1
}

function print_usage {
  die "usage: $0 <target.gpg> [marker]"
}

test -n "$1" || print_usage
test -f "$1" || die "File not found: '$1'"

CONTENT=$(gpg -q -d "$1")
NUMBER_OF_LINES=$(echo "$CONTENT" | wc -l)
if [ "$NUMBER_OF_LINES" -gt 1 ]; then
  test -n "$2" || die "Error: file contains multiple lines and no marker given"
  CONTENT=$(echo "$CONTENT" | grep -i "$2") || die "Error: given marker '$2' not found in file"
  NUMBER_OF_RESULTS=$(echo "$CONTENT" | wc -l)
  test "$NUMBER_OF_RESULTS" -gt 0 || die "Error: given marker '$2' not found in file"
  test "$NUMBER_OF_RESULTS" -lt 2 || die "Error: given marker '$2' not unique"
fi

SECRET=$(echo -n "$CONTENT" | awk -F// '{ print $2 }' | sed -e 's/^ *//g' -e 's/ *$//g')
test -n "$SECRET" || die "Error: failed to find password"
echo -n "$SECRET" | xsel -i -b
