#!/bin/bash

function die {
  echo "$1" 1>&2
  exit 1
}

function print_usage {
  die "usage: $0 <target.gpg>"
}

test -n "$1" || print_usage
test -f "$1" || die "File not found: $1"

TEMP="$1.swp"
OLD="$1.old"
test ! -f "$TEMP" || die "File already existing: $TEMP"
test ! -f "$OLD" || die "File already existing: $OLD"

gpg -q -o "$TEMP" -d "$1"
vim -n "$TEMP"

echo -n "Update the encrypted file $1 [yes/no]? "
while true; do
  read answer
  if [ "$answer" = "yes" ]; then
    mv "$1" "$OLD"
    gpg -o "$1" -e "$TEMP"
    rm -f "$OLD"
    srm -f "$TEMP"
    exit 0
  elif [ "$answer" = "no" ]; then
    srm -f "$TEMP"
    exit 1
  else
    echo -e -n "\nUnable to understand your answer. Please reply with 'yes' or 'no': "
  fi
done
