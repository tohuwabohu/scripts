#!/bin/bash

TEST="test"

rm -rf $TEST
mkdir -p $TEST

echo -n "searching broken links ... "
linklint -no_anchors \
	-quiet \
	-doc "$TEST/results" \
	-http \
	-host "dresdnerssv.local" /@ > "$TEST/linklint.log" 2> "$TEST/linklint.err"

if [ $? -eq 0 ]
	then echo "[OK]"
	else echo "[FAILED]"; exit 1
fi
